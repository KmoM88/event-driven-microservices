# Event-Driven Microservices Backend Sample

Proof of Concept for a scalable Local News Application, based on simplified event-driven microservices architecture and Docker containers. :whale:

Source: https://github.com/rithinch/event-driven-microservices-docker-example

npm 15.11.0

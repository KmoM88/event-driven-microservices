// Init the environment variables and server configurations
require('dotenv').config();

// Import the required packages
const Mongoose = require('mongoose');
const config = require('./environment/config');
const app = require('./app');

// Init Database Connection
// console.log(config.db.uri);
const url_db = `mongodb://${config.db.username}:${config.db.password}@${config.db.hostname}:${config.db.port}/${config.db.table}?authSource=admin`
// console.log(url_db);
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

Mongoose.connect(url_db, options);
let db = Mongoose.connection
db.on('error', console.error.bind(console, "CONNECTION ERROR"));
db.once('open', () => {
  //we're connected!
  console.log('CONNECTION DB OPEN');
})

// Start Listening to Subscribed Events
require('./message-bus/recieve/user.added').start();

// Run the API Server
app.listen(config.port, () => {
  console.log(config.startedMessage);
});
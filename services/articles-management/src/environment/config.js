const config = {
    name: 'Article Management Service',
    baseAPIRoute: 'api',
    port: process.env.PORT || 8080,
    messagebus: process.env.MESSAGE_BUS || 'amqp://rabbitmq',
    environment: process.env.ENVIRONMENT || 'dev',
    db: {
      uri: process.env.MONGO_URI || 'mongodb://mongodb_users:27017/article_management',
      db: process.env.MONGO_DB || 'mongodb',
      port: process.env.MONGO_PORT || '27017',
      hostname: process.env.MONGO_HOSTNAME || 'mongodb',
      username: process.env.DB_USERNAME || 'kmom',
      password: process.env.DB_PASSWORD || 'your_password_kmom',
      table: process.env.DB_TABLE || 'article_management'
    },
    services: {
    },
    messageTimeout: 500,
    jwtsecret: 'yoursecretkey',
  };

  config.startedMessage = `${config.name} is running on port ${config.port}/`;

  module.exports = config;